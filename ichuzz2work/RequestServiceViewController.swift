//
//  RequestServiceViewController.swift
//  ichuzz2work
//
//  Created by JOEL CRAWFORD on 06/12/2019.
//  Copyright © 2019 RedTokens. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import MapKit

class RequestServiceViewController: UIViewController,  UITextFieldDelegate {
    
    @IBOutlet weak var RequestServiceFullName: UITextField!
    @IBOutlet weak var RequestServiceDialCode: UITextField!
    @IBOutlet weak var RequestServicePhoneNumber: UITextField!
    @IBOutlet weak var RequestServiceEmailUserName: UITextField!
    @IBOutlet weak var RequestServiceLocation: UITextField!
    @IBOutlet weak var RequestServicePasswordOne: UITextField!
    @IBOutlet weak var RequestServicePasswordTwo: UITextField!
    
    @IBOutlet weak var RequestServiceSignUpButton: UIButton!
    @IBOutlet weak var RequestServiceTermsAndConditions: UIButton!
    @IBOutlet weak var RequestServicePrivacyPolicy: UIButton!
    @IBOutlet weak var RequestServiceLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
              //==================placeholder color===============
            RequestServiceFullName.attributedPlaceholder = NSAttributedString(string: "Full name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            RequestServiceDialCode.attributedPlaceholder = NSAttributedString(string: "+256",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
           RequestServicePhoneNumber.attributedPlaceholder = NSAttributedString(string: "0712345678",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
           RequestServiceEmailUserName.attributedPlaceholder = NSAttributedString(string: "Email address or username",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            RequestServiceLocation.attributedPlaceholder = NSAttributedString(string: "Click to select location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            RequestServicePasswordOne.attributedPlaceholder = NSAttributedString(string: "Login password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            RequestServicePasswordTwo.attributedPlaceholder = NSAttributedString(string: "Re-enter password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        //=====Gesture to close keyboard on outside tap ====
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        //============UIDELEGATE===================
         RequestServiceFullName.delegate = self
         RequestServiceDialCode.delegate = self
         RequestServicePhoneNumber.delegate = self
         RequestServiceEmailUserName.delegate = self
         RequestServiceLocation.delegate = self
         RequestServicePasswordOne.delegate = self
         RequestServicePasswordTwo.delegate = self

        // Do any additional setup after loading the view.
    }
    
    //=======move to next textfield on return or enter press=======
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case RequestServiceFullName:RequestServicePhoneNumber.becomeFirstResponder()
        case RequestServicePhoneNumber:RequestServiceEmailUserName.becomeFirstResponder()
        case RequestServiceEmailUserName:RequestServiceLocation.becomeFirstResponder()
        case RequestServiceLocation:RequestServicePasswordOne.becomeFirstResponder()
        case RequestServicePasswordOne:RequestServicePasswordTwo.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    //=========Request a service signup button======
    @IBAction func RequestServiceSignUpBtn(_ sender: UIButton) {
         //========chcecking for the fields so that they are not empty =============
        if (RequestServiceFullName.text?.isEmpty)! ||
            (RequestServicePhoneNumber.text?.isEmpty)! ||
            (RequestServiceLocation.text?.isEmpty)! ||
            (RequestServicePasswordOne.text?.isEmpty)! ||
            (RequestServicePasswordTwo.text?.isEmpty)!
        {
            //=========display alert messages and return============
                  displayMessage(userMessage: "All fields are required")
                       return
                   }
        
        
        
    }
    
    //=========Request a service Terms and conditions Button====
    @IBAction func RequestServiceTermsAndConditionsBtn(_ sender: UIButton) {
        if let url = URL(string: "https://www.google.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    //=========Request a service privacy policy Button====
    @IBAction func RequestServicePrivacyPolicy(_ sender: UIButton) {
        if let url = URL(string: "https://www.google.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
     
    //========RequestService login in button======
    @IBAction func RequestServiceLoginBtn(_ sender: UIButton) {
    }
    
    
    //=======function to remove activity indicator=========
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView)
    {
        DispatchQueue.main.async
         {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
        }
    }
    
    

    //============function to display alert messages==================
    func displayMessage(userMessage: String) -> Void {
        DispatchQueue.main.async
            {
                let alertController = UIAlertController (title: "Alert", message: userMessage, preferredStyle: .alert)
        
                let OKAction = UIAlertAction(title: "OK", style: .default) {
                    (action: UIAlertAction!) in
                    //==========code in this block will trigger when ok button is taped============
                    print("Ok button tapped")
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//=============Extension to the UITextfield for setting Left and Right padding=========
extension UITextField {
    func setLeftPaddingPointsRequestService(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPointsRequestService(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
