//
//  MapVC.swift
//  ichuzz2work
//
//  Created by JOEL CRAWFORD on 02/01/2020.
//  Copyright © 2020 RedTokens. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit


class MapVC: UIViewController {
    var servicelocationnameText = ""

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var serviceproviderlabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //=====adding map view initial setting,google maps SDK compass ===
        mapView.settings.compassButton = true
        //=====google map user location==============
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    //============passsing data to Service Provider location textfield======
    @IBAction func serviceproviderlocationpassdatatotextfield(_ sender: UIButton) {
        self.servicelocationnameText = serviceproviderlabel.text!
        performSegue(withIdentifier: "ServiceProvider", sender: self)
        
    }
    //============prepare for segue to present the service provider view controller======
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ServiceProviderViewController
        vc.finalLocation = self.servicelocationnameText
    }
    
    //======servive provider location on textfield tap=====
    @IBAction func ServiceProviderLocationTapped(_ sender: UITextField) {
        gotoPlaces()
    }
    
    
    //======function that goes to google places on textfield search click======

    func gotoPlaces() {
         txtSearch.resignFirstResponder()
                      let acController = GMSAutocompleteViewController()
                      acController.delegate = self
                      present(acController, animated: true, completion: nil)
        
    }
}

extension MapVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
           print("locations = \(locValue.latitude) \(locValue.longitude)")
       
    }
}

extension MapVC: GMSAutocompleteViewControllerDelegate {
     //============auto added functio after adding the GMSGeoder====
       func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
           
       }
       
       func wasCancelled(_ viewController: GMSAutocompleteViewController) {
           
       }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        // =======Get the place name from 'GMSAutocompleteViewController'=====
        //====Then display the name in textField=========
        
        print("Place name: \(String(describing: place.name))")
        //============ Dismiss the GMSAutocompleteViewController when something is selected ============
            dismiss(animated: true, completion: nil)
            
            self.mapView.clear()
            self.txtSearch.text = place.name
        
            let cord2D = CLLocationCoordinate2D(latitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude))
        
        //=========Reverse Geocoder to convert latitude and longitude  to address names======
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(cord2D) { (placemarkers, error) -> Void in
            
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
                
            } else {
                    if let places = placemarkers?.results() {
                        if let place = places.first {
                            if let lines = place.lines {
                            print("GEOCODE: Formatted Address: \(lines)")
                               
                                let thoroughfare = place.thoroughfare ?? ""
                                let subLocality = place.subLocality ?? ""
                                let country = place.country ?? ""
                                
                           //=================printing latitude and longitude values============
                                let latitude = place.coordinate.latitude
                                let longitude =  place.coordinate.longitude
                                
                               self.serviceproviderlabel.text =  " \(thoroughfare), \r \(subLocality), \r \(country), \r \(longitude), \r \(latitude)"
                                
                        }
                            
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                        
                } else {
                    print("GEOCODE: nil in places")
                }
                
                }
            

            //========creating a GMSMarker object==============
                        let marker = GMSMarker()
                        marker.position =  cord2D
                        marker.title = "Location"
                        marker.snippet = place.name
            
        
        //=======setting custome image for the marker====
                    let markerImage = UIImage(named: "icon_offer_pickup")!
                    let markerView = UIImageView(image: markerImage)
                    marker.iconView = markerView
                    marker.map = self.mapView
        
            //===========setting zoom=============================
            self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        //=======================================handle error=======================
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //====== Dismiss when the user canceled the action======
        dismiss(animated: true, completion: nil)
        
    }
    }
    
}

