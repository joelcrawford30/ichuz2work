//
//  TogglePassword.swift
//  ichuzz2work
//
//  Created by JOEL CRAWFORD on 06/12/2019.
//  Copyright © 2019 RedTokens. All rights reserved.
//

import UIKit

class TogglePassword: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    let rightButton  = UIButton(type: .custom)

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
        }

        required override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
        }

        func commonInit() {
            rightButton.setImage(UIImage(named: "password_show") , for: .normal)
            rightButton.addTarget(self, action: #selector(toggleShowHide), for: .touchUpInside)
            rightButton.frame = CGRect(x:0, y:0, width:30, height:30)

            rightViewMode = .always
            rightView = rightButton
            isSecureTextEntry = true
        }

        @objc
        func toggleShowHide(button: UIButton) {
            toggle()
        }

        func toggle() {
            isSecureTextEntry = !isSecureTextEntry
            if isSecureTextEntry {
                rightButton.setImage(UIImage(named: "view-password") , for: .normal)
            } else {
                rightButton.setImage(UIImage(named: "hide-password") , for: .normal)
            }
        }

    }
