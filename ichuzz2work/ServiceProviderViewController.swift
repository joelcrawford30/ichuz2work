//
//  ServiceProviderViewController.swift
//  ichuzz2work
//
//  Created by JOEL CRAWFORD on 06/12/2019.
//  Copyright © 2019 RedTokens. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import MapKit

class ServiceProviderViewController: UIViewController, UITextFieldDelegate {
    var finalLocation = ""
    let locationManager = CLLocationManager()
  
    @IBOutlet weak var ServiceProviderFullName: UITextField!
    @IBOutlet weak var ServiceProviderDialCode: UITextField!
    @IBOutlet weak var ServiceProviderNumber: UITextField!
    @IBOutlet weak var ServiceProviderEmailorUserName: UITextField!
    @IBOutlet weak var ServiceProviderLocation: UITextField!
    @IBOutlet weak var ServiceProviderPassworOne: UITextField!
    @IBOutlet weak var ServiceProviderPasswordTwo: UITextField!
   

    @IBOutlet weak var ServiceProviderSignUpButton: UIButton!
    @IBOutlet weak var ServiceProviderTermsConditions: UIButton!
    @IBOutlet weak var ServiceProviderPrivacyPolicy: UIButton!
    @IBOutlet weak var ServiceProvideAccountLogin: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        ServiceProviderLocation.text = finalLocation
        
        ServiceProviderLocation.addTarget(self, action: #selector(getCurrentLocation), for: UIControl.Event.editingDidBegin)
        
        
        //==================placeholder color===============
     ServiceProviderFullName.attributedPlaceholder = NSAttributedString(string: "Full name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderDialCode.attributedPlaceholder = NSAttributedString(string: "+256",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderNumber.attributedPlaceholder = NSAttributedString(string: "0712345678",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderEmailorUserName.attributedPlaceholder = NSAttributedString(string: "Email address or username",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderLocation.attributedPlaceholder = NSAttributedString(string: "Click to select location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderPassworOne.attributedPlaceholder = NSAttributedString(string: "Login password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
     ServiceProviderPasswordTwo.attributedPlaceholder = NSAttributedString(string: "Re-enter password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
           

       //=====Gesture to close keyboard on outside tap ====
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
        //============UIDELEGATE===
        ServiceProviderFullName.delegate = self
        ServiceProviderDialCode.delegate = self
        ServiceProviderNumber.delegate = self
        ServiceProviderEmailorUserName.delegate = self
        ServiceProviderLocation.delegate = self
        ServiceProviderPassworOne.delegate = self
        ServiceProviderPasswordTwo.delegate = self
    }
    
    
    
   
    //============GOOGLE MAP, launching google map on ServiceProviderLocation Textfield Tapped========
    @IBAction func ServiceProviderLocation(_ sender: UITextField) {
        //        if (sender.text != nil) {
        //            displayMessage(userMessage: "Current location used")
        //        } else {
        //            getCurrentLocation()
        //        }
        //
                if let text = sender.text, text.isEmpty {
                    getCurrentLocation()
                } else {
                    displayMessage(userMessage: "Current location used")
                }

    }
    
    
    
    //======function to get user location===========
    @objc func getCurrentLocation() {
        
//        //==============Check for empty ServiceProviderLocation TextField============
//        if let text = ServiceProviderLocation.text, text.isEmpty {
//
        //====ask for authorisation from the user===
        self.locationManager.requestAlwaysAuthorization()

        //=================for use in foreground====================
        self.locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = (self as CLLocationManagerDelegate)
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
        
//        } else {
//            // ===========ServiceProviderLocation is NOT Empty ======= open a google map ===
//            displayMessage(userMessage: "Your current location will be used")
//            return
//
//        }
    }
    
    //=======move to next textfield on return or enter press=======
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case ServiceProviderFullName: ServiceProviderNumber.becomeFirstResponder()
        case ServiceProviderNumber: ServiceProviderEmailorUserName.becomeFirstResponder()
        case ServiceProviderEmailorUserName: ServiceProviderLocation.becomeFirstResponder()
        case ServiceProviderLocation: ServiceProviderPassworOne.becomeFirstResponder()
        case ServiceProviderPassworOne: ServiceProviderPasswordTwo.becomeFirstResponder()
        default:
             textField.resignFirstResponder()
        }
        return false
    }
    
    
    @IBAction func ServiceProviderSignUpBtn(_ sender: Any) {
        
        //========chcecking for the fields so that they are not empty =============
        if (ServiceProviderFullName.text?.isEmpty)! ||
        (ServiceProviderNumber.text?.isEmpty)! ||
        (ServiceProviderEmailorUserName.text?.isEmpty)! ||
         (ServiceProviderLocation.text?.isEmpty)! ||
         (ServiceProviderPassworOne.text?.isEmpty)!
       {
            //=========display alert messages and return============
       displayMessage(userMessage: "All fields are required")
            return
        }
        
        //================checking if the password is secured==================
        let cleanedpassword1 = ServiceProviderPassworOne.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let cleanedpassword2 = ServiceProviderPasswordTwo.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if  Utilities.isValidPassword(testStr: cleanedpassword1) == false && Utilities.isValidPassword(testStr: cleanedpassword2) {
            //=========password isnt secured enough=======
            displayMessage(userMessage: "Password isn't secured enough")
            return
        }
        
        //========================checking for match password=====================
        if Utilities.isPasswordSame(password: cleanedpassword1, confirmPassword: cleanedpassword2) == false {
            //========alert messages ===========
            displayMessage(userMessage: "Passwords do not match")
            return
    
        }
        //=========checking for password length=======
        if Utilities.isPwdLenth(password: cleanedpassword1, confirmPassword: cleanedpassword2) == false {
            //======alert messages======
            displayMessage(userMessage: "Passwords should be more than 6 characters")
            return
        }
        
        //====checking for email and its format================
        let cleanedemail = ServiceProviderEmailorUserName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isValidEmail(testStr: cleanedemail) == false {
            //==========alert messgae=========
            displayMessage(userMessage: "Please enter a valid email or username")
            return
        }
        
        //===========Create Activity Indicator=========
        let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
              
              //===========Position Activity Indicator in the center of the main view ===========
              myActivityIndicator.center = view.center
              
              //===========If needed, you can prevent Acivity Indicator from hiding when stopAnimating() is called=========
              myActivityIndicator.hidesWhenStopped = false
              
              //===========Start Activity Indicator============
              myActivityIndicator.startAnimating()
        
        //Call stopAnimating() when need to stop activity indicator
        //myActivityIndicator.stopAnimating()
        
               view.addSubview(myActivityIndicator)

        //===========sending an http request to register service provider===========
        let serviceProviderurl = URL(string: "https://ichuzz2work.com/register")
        var serviceRequest = URLRequest(url: serviceProviderurl!)
        
        serviceRequest.httpMethod = "POST" //compose a query string
        serviceRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        serviceRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
     
        //==========json payload creation==========
        let  ServiceProviderPostString = ["providerfullname": ServiceProviderFullName.text!,
                                          "providerdialcode": ServiceProviderDialCode.text!,
                                          "providernumber": ServiceProviderNumber.text!,
                                          "providerusernameoremail": ServiceProviderEmailorUserName.text!,
                                          "providerlocation": ServiceProviderLocation.text!,
                                          "providerpassword": ServiceProviderPassworOne.text!] as [String: String]
        
        //========serialization====You'll need to Serialize the response data to get the raw JSON=========
        do {
            serviceRequest.httpBody = try JSONSerialization.data(withJSONObject: ServiceProviderPostString, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            displayMessage(userMessage: "Something went wrong. Try again.")
            return
        }
        
        //========sending http request==============
        let task = URLSession.shared.dataTask(with: serviceRequest) {(data: Data?, response: URLResponse?, error: Error?) in
        
        self.removeActivityIndicator(activityIndicator: myActivityIndicator)
        
        if error != nil
        {
            self.displayMessage(userMessage: "Could not successfully perform this request. Please try again later")
            print("error=\(String(describing: error))")
            return
        }

            //=====Let's convert response sent from a server side code to a NSDictionary object: =========
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                if let parseJSON = json {
                    let userId = parseJSON["userId"] as? String
                    print("User id: \(String(describing: userId!))") /// =============TODO gives error===========
                    
                    if (userId?.isEmpty)! {
                        //===================display alert box==========
                        self.displayMessage(userMessage: "could not succesfully perform this request. please try again later")
                        return
                        
                    } else {
                        self.displayMessage(userMessage: "Successfully registered a new account. please proceed to login") }
                    
                    } else {
                    
                    //===========Display an Alert dialog with a friendly error message=========
                    self.displayMessage(userMessage: "Could not successfully perform this request. Please try again later")
                    }
                    
            } catch {
                self.removeActivityIndicator(activityIndicator: myActivityIndicator)
                print(error)
            }
            
            }
            
            task.resume()
            }
    
    
    
        
    //==========service provider Terms and conditions function =========
    @IBAction func ServiceProviderTermsandConditions(_ sender: Any) {
        if let url = URL(string: "https://www.google.com/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    //============service provider Privacy policy function============
    @IBAction func ServiceProviderPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "https://www.google.com/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    //=======function to remove activity indicator=========
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView)
    {
        DispatchQueue.main.async
         {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
        }
    }
    
    

    //============function to display alert messages==================
    func displayMessage(userMessage: String) -> Void {
        DispatchQueue.main.async
            {
                let alertController = UIAlertController (title: "Alert", message: userMessage, preferredStyle: .alert)
        
                let OKAction = UIAlertAction(title: "OK", style: .default) {
                    (action: UIAlertAction!) in
                    //==========code in this block will trigger when ok button is taped============
                    print("Ok button tapped")
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
   
}

//========================CLLocation Manager Delegate==============
extension ServiceProviderViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
           guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

           print("locations = \(locValue.latitude) \(locValue.longitude)")
       
    }
}

//=============Extension to the UITextfield for setting Left and Right padding=========
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
