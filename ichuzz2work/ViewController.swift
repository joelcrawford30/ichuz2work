//
//  ViewController.swift
//  ichuzz2work
//
//  Created by JOEL CRAWFORD on 04/12/2019.
//  Copyright © 2019 RedTokens. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    

    @IBOutlet weak var emailusernamelogin: UITextField!
    @IBOutlet weak var loginpassword: UITextField!
    
    @IBOutlet weak var signinbutton: UIButton!
    @IBOutlet weak var createaccountbutton: UIButton!
    @IBOutlet weak var resetpasswordbutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //==================placeholder color===============
            emailusernamelogin.attributedPlaceholder = NSAttributedString(string: "Email address or username",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            loginpassword.attributedPlaceholder = NSAttributedString(string: "Login password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        //=====Gesture to close keyboard on outside tap ====
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        //======UIDELEGATE FOR NEXT TEXTFIELD ON RETURN PRESS==
        emailusernamelogin.delegate = self
        loginpassword.delegate = self
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailusernamelogin: loginpassword.becomeFirstResponder()
            
        default:
           loginpassword.resignFirstResponder()
        }
        return true
    }
    @IBAction func signinbtn(_ sender: Any) {
    }
    
    @IBAction func resetpasswordbtn(_ sender: Any) {
        guard let url = URL(string: "https://api.ichuzz2work.com/password/reset") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    

}

